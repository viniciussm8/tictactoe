/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;
import java.util.*;
/**
 *
 * @author ltortelli
 */
public class MinMax {

    protected ArrayList<Tabuleiro> max;
    protected ArrayList<Tabuleiro> min;
    protected Tabuleiro estadoAtual;
    private Queue<Tabuleiro> caminhoResolucao;
    protected int numeroIteracoes;
    private int profundidadeMaxima;
    Interface inter;
    
    
    public MinMax(){
        
        this.estadoAtual = new Tabuleiro();
        this.max = new ArrayList<Tabuleiro>();
        this.min = new ArrayList<Tabuleiro>();
        this.caminhoResolucao = new LinkedList<Tabuleiro>();
        this.numeroIteracoes = 0;
        this.profundidadeMaxima = 1;
    }
    
    public MinMax(int profundidadeRequerida, boolean a){

        if(profundidadeRequerida%2==0){
            profundidadeRequerida++;
        }
        this.estadoAtual = new Tabuleiro();
        this.max = new ArrayList<Tabuleiro>();
        this.min = new ArrayList<Tabuleiro>();
        
        this.caminhoResolucao = new LinkedList<Tabuleiro>();
        this.numeroIteracoes = 0;
        this.profundidadeMaxima = profundidadeRequerida;
        inter = new Interface(a);
        inter.setVisible(true);
    }
    
   

    
    public void metodoMinMax(boolean playerS){
        int indice;
        int cont=0;
        boolean player=playerS;
        Tabuleiro resposta = this.estadoAtual;
        while((cont<this.profundidadeMaxima)){
            // Significa que o jogo esta como player 1
            if(player==true){
                if(this.min.isEmpty()){
                    for(indice=0;indice<9;indice++){

                        //Cria novo tabuleiro, passando o tabuleiroAtual para realizar as operações
                        Tabuleiro novoTabuleiro = new Tabuleiro(this.estadoAtual);
                        novoTabuleiro.alteraEstado(indice,true);
                        
                        // Ira setar o custo do novo tabuleiro criado, com base na heuristica
                        novoTabuleiro.setCusto();
                        if(this.estadoAtual.getValorIndice(indice)!='X'){
                            this.max.add(novoTabuleiro);
                        }
                    }
                }
                else{
                    int i=0;
                    Tabuleiro aux;
                    for(i=0;i<this.min.size();i++){
                        aux = this.min.get(i);
                        for(indice=0;indice<9;indice++){

                            //Cria novo tabuleiro, passando o tabuleiroAtual para realizar as operações
                            Tabuleiro novoTabuleiro = new Tabuleiro(aux);
                            novoTabuleiro.alteraEstado(indice,true);
                            //novoTabuleiro.imprimeTabuleiro();
                            // Ira setar o custo do novo tabuleiro criado, com base na heuristica
                            novoTabuleiro.setCusto();
                            if(this.estadoAtual.getValorIndice(indice)!='X'){
                                this.max.add(novoTabuleiro);
                            }
                        }
                    }
                    this.min.clear();
                }  
            }
            else if(player==false){
                if(this.max.isEmpty()){
                    for(indice=0;indice<9;indice++){

                        //Cria novo tabuleiro, passando o tabuleiroAtual para realizar as operações
                        Tabuleiro novoTabuleiro = new Tabuleiro(this.estadoAtual);
                        novoTabuleiro.alteraEstado(indice,false);

                        // Ira setar o custo do novo tabuleiro criado, com base na heuristica
                        novoTabuleiro.setCusto();
                        if(this.estadoAtual.getValorIndice(indice)!='O'){
                            this.min.add(novoTabuleiro);
                        }
                    }
                }
                else{
                    int i=0;
                    Tabuleiro aux;
                    for(i=0;i<this.max.size();i++){
                        aux = this.max.get(i);
                        for(indice=0;indice<9;indice++){

                            //Cria novo tabuleiro, passando o tabuleiroAtual para realizar as operações
                            Tabuleiro novoTabuleiro = new Tabuleiro(aux);
                            novoTabuleiro.alteraEstado(indice,false);

                            // Ira setar o custo do novo tabuleiro criado, com base na heuristica
                            novoTabuleiro.setCusto();
                            
                            if(this.estadoAtual.getValorIndice(indice)!='O'){
                                this.min.add(novoTabuleiro);
                            }
                        }
                    }
                    this.max.clear();
                }
                              
            }
            if(player==true){player=false;}
            else if(player==false){player=true;}
            cont++;
        }
        
        
        
        if(player==true){
            Collections.sort(this.min, new ComparadorConflitos());
            resposta = this.min.remove(0);
            
            
        }
        else if(player == false){
            Collections.sort(this.max, new ComparadorConflitosMais());
            resposta = this.max.remove(0);
        }
        this.max.clear();
        this.min.clear();
        this.estadoAtual = resposta.getPaiProfundidade(resposta,cont-1);
      
        this.caminhoResolucao.add(this.estadoAtual);
        
    }
    
    public void CpuVsCpu(){
        inter.Log.setText("# Computer vs. Computer #\n");

        inter.ativeClean();


        boolean findAnswer=true;
        while(findAnswer){
           
            //System.out.print(this.numeroIteracoes+"\n");
            this.metodoMinMax(true);
            this.metodoMinMax(false);
            this.numeroIteracoes+=2;
            if(this.numeroIteracoes==8){findAnswer=false;}
            else{
                findAnswer = !this.estadoAtual.checkAnswer();}
        }
      this.mostraCaminhoLog();
      
    }
    
    public boolean PlayerVsCpu(int indice){
      
        this.estadoAtual.alteraEstado(indice,true);
        this.metodoMinMax(false);
        this.estadoAtual.imprimeTabuleiro();
//        inter.attTab(estadoAtual.getTab());
        return this.estadoAtual.checkAnswer();
        
    }
    
    public void mostraCaminhoLog(){

         while(!this.caminhoResolucao.isEmpty()){

             Tabuleiro aux;

             aux = this.caminhoResolucao.poll();

             aux.imprimeTabuleiro();

             inter.attTab(aux.getTab());

         }

     }


    
    public void mostraCaminho(){
        while(!this.caminhoResolucao.isEmpty()){
            Tabuleiro aux;
            aux = this.caminhoResolucao.poll();
            aux.imprimeTabuleiro();
            System.out.println();
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
//    public static void main(String[] args) {
//        MinMax teste = new MinMax(1);
//        int indice,i=0;
//        boolean findAnswer=true;
//        Scanner opcao = new Scanner(System.in);
//        
//        while((i<4)&&(findAnswer)){
//            indice = opcao.nextInt();
//            findAnswer = !teste.PlayerVsCpu(indice);
//            i++;
//        }
//        
//    }
    
}
