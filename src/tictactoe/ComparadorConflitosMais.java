/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

import java.util.Comparator;

/**
 *
 * @author Lucas
 */

class ComparadorConflitosMais implements Comparator<Tabuleiro> {  
    
    public int compare(Tabuleiro t, Tabuleiro t1) {
        if(t.custo > t1.custo){
            return -1;
        }
        else if(t.custo < t1.custo){
            return 1;
        }
        else 
            return 0;
        
    }
}